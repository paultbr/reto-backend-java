# Reto Backend
API Rest que permite realizar una rotación antihoraria a una matriz NxN.

## Tecnologias
- Spring Boot con **Java v8**
- Docker 

## Como inicializar el API Rest

Abrimos nuestro terminal y nos ubicamos en la raiz del proyecto, ya en la raiz y ademas que el proyecto esta dockerizado creamos la imagen con el siguiente comando :
`docker build . -t retoback:v0.0.1`

Luego inicializamos la imagen con el siguiente comando:
`docker run --name retoback -p 8080:8080 retoback:v0.0.1`

> El Backend corre en el puerto 8080

## Prueba del API Rest
En postman o en cualquier otra herramienta de pruebas de api, ingresamos la siguiente ruta con el método POST:

`POST` : localhost:8080/api/retoback/rotarMatriz

Y enviaremos la matriz que se desea rotar (en caso de postman en el apartado de “Body” seleccionamos “raw” y colocamos el formato que deseamos enviar que sería JSON)
y colacamos la matriz que se desea invertir.

> Se coloca la matriz sin ninguna etiqueta como en el ejemplo siguiente

Ejemplo:

[ [1,2,3], [4,5,6], [7,8,9] ]

Como respuesta obtendremos la matriz rotada.

[ [3,6,9], [2,5,8], [1,4,7] ]

###### Un ejemplo visual

![alt tag](imagenes/Ejemplo.JPG)

### Porque se utilizó Spring Boot 
Por la facilidad y rapidez al desarrollar aplicaciones web y microservicios. Otra ventaja es el proporcinar funciones listas para producción, como controles de estado, métricas, etc.