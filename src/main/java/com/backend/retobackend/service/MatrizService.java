package com.backend.retobackend.service;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.backend.retobackend.service.interfaces.IMatrizService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.backend.retobackend.model.Result;

@Service
public class MatrizService implements IMatrizService{
	
	public Result rotarMatriz(int [][] data) {
		try {
			int tamanio = data.length;//tamanio de la matriz inicial
			int[][] nuevamatriz = new int[tamanio][tamanio];// creamos la nueva matriz que estara rotada 
			//Se recorre la matriz origen con dos bucles, obteniendo en cada iteracion las posiciones x,y que se colocaran en la nueva matriz
			for (int x=0;x<tamanio;x++) {
				  for (int y=0;y<tamanio;y++) {
				    nuevamatriz[tamanio-1-y][x] = data[x][y];
				  }
			}
			return new Result(0, true, "ok",nuevamatriz);
		}
		catch (Exception e) {
			return new Result(1, false, e.getMessage().toString());
		}
		
	}
}
