package com.backend.retobackend.service.interfaces;

import org.springframework.http.ResponseEntity;
import com.backend.retobackend.model.Result;

public interface IMatrizService {
	
	public Result rotarMatriz(int [][] Data);
}
