package com.backend.retobackend.controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.backend.retobackend.model.Result;
import com.backend.retobackend.service.MatrizService;
import com.backend.retobackend.service.interfaces.IMatrizService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/retoback")
public class MatrizController {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(MatrizController.class); 
	
	@Autowired
	private IMatrizService matrizService;
	
	@PostMapping(value="/rotarMatriz",produces="application/json", consumes = "application/json")
	public Result rotarMatriz( @RequestBody int [][] data) {
		try {
			return matrizService.rotarMatriz(data);
		}
		catch (Exception e) {
			registrarError(e);
			return new Result(1, false, e.getMessage().toString());
		}
	}
	
	private void registrarError(Exception ex) {
		ex.printStackTrace();
		LOGGER.error(ex.getMessage());
	}
	
}
