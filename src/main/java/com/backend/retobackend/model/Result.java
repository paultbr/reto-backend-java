package com.backend.retobackend.model;

public class Result {
	
	private int code;
	private boolean success;
	private String msg;
	private Object data;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	public Result(int codeParameter, boolean successParameter, String messageParameter) {
		this.setCode(codeParameter);
		this.setSuccess(successParameter);
		this.setMsg(messageParameter);
	}
	public Result(int codeParameter, boolean successParameter, String messageParameter,Object resultParameter) {
		this.setCode(codeParameter);
		this.setSuccess(successParameter);
		this.setMsg(messageParameter);
		this.setData(resultParameter);
	}

}
