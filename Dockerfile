FROM fabric8/java-alpine-openjdk8-jdk
COPY "./target/retobackend-0.0.1-SNAPSHOT.jar" "retobackend.jar"
ENTRYPOINT ["java", "-jar", "retobackend.jar"]